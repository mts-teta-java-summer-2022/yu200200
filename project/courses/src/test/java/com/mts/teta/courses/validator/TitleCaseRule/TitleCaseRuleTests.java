package com.mts.teta.validator.TitleCaseRule;

import com.mts.teta.validator.TitleCaseCheck;
import com.mts.teta.validator.TitleCaseLang;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TitleCaseRuleTests {

    private TitleCaseCheck chkANY;
    private TitleCaseCheck chkEN;
    private TitleCaseCheck chkRU;

    private String substTilte(String s) {
        return s.replace("%s", "Title");
    }

    //выполнение теста, который должен вернуть определенную ошибку
    private void doTestBad(
        TitleCaseCheck  testObj,            // класс, используемый для проверки
        String          testStr,            // валидируемая строка
        String          expectedError       // сообщение об ошибке
    )
    {
        String realError = testObj.doCheck(testStr);
        // если realError содержит в качестве подстроки ожидаемое сообщение, тест успешен
        Assertions.assertTrue(realError.contains(expectedError));
    }

    private void doTestGood(
            TitleCaseCheck  testObj,            // класс, используемый для проверки
            String          testStr            // валидируемая строка
    ) {
        String realError = testObj.doCheck(testStr);
        // если realError содержит в качестве подстроки ожидаемое сообщение, тест успешен
        Assertions.assertTrue(realError.isEmpty());
    }

    @BeforeEach
    void setUp() {
        chkANY = new TitleCaseCheck(TitleCaseLang.ANY);
        chkEN = new TitleCaseCheck(TitleCaseLang.EN);
        chkRU = new TitleCaseCheck(TitleCaseLang.RU);
    }

    //@Test
    @Test
    void testGood () {
        doTestGood(chkANY, "Some Title SomeTitle Two");
        doTestGood(chkEN, "Some Title SomeTitle Two");
        doTestGood(chkEN, "Some Title but SomeTitle or Two");
        doTestGood(chkEN, "Or Title, Title A");
        doTestGood(chkEN, "Or Title an Title A");
        doTestGood(chkRU, "Название название два");
    }

    @Test
    void testCRLFTAB () {
        String currentError = substTilte(TitleCaseErrors.CRLFTAB);

        doTestBad(chkANY, "Some Title\n", currentError);
        doTestBad(chkANY, "Some Title\r", currentError);
        doTestBad(chkANY, "Some Title\tSomeTitle Two ", currentError);

        doTestBad(chkEN, "Some Title\n", currentError);
        doTestBad(chkEN, "Some Title\r", currentError);
        doTestBad(chkEN, "Some Title\tSomeTitle Two", currentError);

        doTestBad(chkRU, "Название\n", currentError);
        doTestBad(chkRU, "Название\r", currentError);
        doTestBad(chkRU, "Название\tЕще Название", currentError);
    }

    @Test
    void testTwoSpace() {
        String currentError = substTilte(TitleCaseErrors.twoSpaces);

        doTestBad(chkANY, "Some  Title", currentError);
        doTestBad(chkANY, "Some   Title", currentError);

        doTestBad(chkEN, "Some  Title", currentError);
        doTestBad(chkEN, "Some   Title", currentError);

        doTestBad(chkRU, "Какое то   название", currentError);
        doTestBad(chkRU, "Тоже     какое то, название", currentError);
    }

    @Test
    void testStartEndSpace() {
        String currentErrorStart = substTilte(TitleCaseErrors.startSpace);
        String currentErrorEnd = substTilte(TitleCaseErrors.endSpace);

        doTestBad(chkANY, " Some Title", currentErrorStart);
        doTestBad(chkANY, "Some Title ", currentErrorEnd);
        doTestBad(chkANY, " Some Title ", currentErrorStart);
        doTestBad(chkANY, " Some Title ", currentErrorEnd);
        doTestBad(chkANY, "    Some Title    ", currentErrorStart);
        doTestBad(chkANY, "    Some Title    ", currentErrorEnd);

        doTestBad(chkEN, " Some Title", currentErrorStart);
        doTestBad(chkEN, "Some Title ", currentErrorEnd);
        doTestBad(chkEN, " Some Title ", currentErrorStart);
        doTestBad(chkEN, " Some Title ", currentErrorEnd);
        doTestBad(chkEN, "    Some Title    ", currentErrorStart);
        doTestBad(chkEN, "    Some Title    ", currentErrorEnd);

        doTestBad(chkRU, " Какое то   название", currentErrorStart);
        doTestBad(chkRU, "Тоже какое то, название ", currentErrorEnd);
        doTestBad(chkRU, " Тоже какое то, название ", currentErrorStart);
        doTestBad(chkRU, " Тоже какое то, название ", currentErrorEnd);
        doTestBad(chkRU, "   Тоже какое то, название   ", currentErrorStart);
        doTestBad(chkRU, "   Тоже какое то, название   ", currentErrorEnd);
    }

    @Test
    void testMixLangs() {
        String currentError = substTilte(TitleCaseErrors.mixLangs);

        doTestBad(chkANY, "Some Title название", currentError);
        doTestBad(chkANY, "Название Some Title", currentError);
        doTestBad(chkANY, "Some \"Название\" Title", currentError);
        doTestBad(chkANY, "Название какое, Title", currentError);

        doTestBad(chkEN, "Some Title название", currentError);
        doTestBad(chkEN, "Название Some Title", currentError);
        doTestBad(chkEN, "Some \"Название\" Title", currentError);
        doTestBad(chkEN, "Название какое, Title", currentError);

        doTestBad(chkRU, "Some Title название", currentError);
        doTestBad(chkRU, "Название Some Title", currentError);
        doTestBad(chkRU, "Some \"Название\" Title", currentError);
        doTestBad(chkRU, "Название какое, Title", currentError);
    }

    @Test
    void testBadPunctuation() {
        String currentErrorQuote = substTilte(TitleCaseErrors.punctMotBeforeAfter);
        String currentErrorComma = substTilte(TitleCaseErrors.punctMotAfter);

        doTestBad(chkANY, "Some ,Title", currentErrorComma);
        doTestBad(chkANY, "Some, Some ,Title ", currentErrorComma);
        doTestBad(chkANY, "Some, Some : Title ", currentErrorComma);
        doTestBad(chkANY, "Some \" some\" Title", currentErrorQuote);
        doTestBad(chkANY, "Title Title ' Title", currentErrorQuote);

        doTestBad(chkEN, "Some ,Title", currentErrorComma);
        doTestBad(chkEN, "Some, Some ,Title ", currentErrorComma);
        doTestBad(chkEN, "Some, Some : Title ", currentErrorComma);
        doTestBad(chkEN, "Some \" some\" Title", currentErrorQuote);
        doTestBad(chkEN, "Title Title ' Title", currentErrorQuote);

        doTestBad(chkRU, "Некое ,название", currentErrorComma);
        doTestBad(chkRU, "Некое, некое ,название ", currentErrorComma);
        doTestBad(chkRU, "Какое, такое : название ", currentErrorComma);
        doTestBad(chkRU, "Еще \" тоже\" название", currentErrorQuote);
        doTestBad(chkRU, "Название Титул ' Титул", currentErrorQuote);
    }

    @Test
    void testBadChars() {
        String currentError = substTilte(TitleCaseErrors.badChars);

        doTestBad(chkANY, "Some+ Title", currentError);
        doTestBad(chkANY, "Some 4 Some, 2 Title", currentError);
    }

    @Test
    void testBadCharsEN() {
        String currentError = substTilte(TitleCaseErrors.badCharsEN);

        doTestBad(chkEN, "Some+ Title", currentError);
        doTestBad(chkEN, "Some 4 Some, 2 Title", currentError);
    }

    @Test
    void testNoCapInBegAndEndEN() {
        String currentError = substTilte(TitleCaseErrors.noFirstLastCapsEN);

        doTestBad(chkEN, "Some title", currentError);
        doTestBad(chkEN, "some Title", currentError);
        doTestBad(chkEN, "title", currentError);
        doTestBad(chkEN, "some Some Some Title", currentError);
        doTestBad(chkEN, "Some Some Some title", currentError);
        doTestBad(chkEN, "some Some Some title", currentError);
    }

    @Test
    void testNoCapExceptPrepEN() {
        String currentError = substTilte(TitleCaseErrors.noAllExceptPrepCapsEN);

        doTestBad(chkEN, "Some title Yes", currentError);
        doTestBad(chkEN, "Some but small Title", currentError);
        doTestBad(chkEN, "some Some Some some Title", currentError);
    }

    @Test
    void testBadCharsRU() {
        String currentError = substTilte(TitleCaseErrors.badCharsRU);

        doTestBad(chkRU, "Какое-то название", currentError);
        doTestBad(chkRU, "Тоже название. с цифрами 123", currentError);
    }

    @Test
    void testNoFirstCapOtherSmallRU() {
        String currentErrorNoFirstCap = substTilte(TitleCaseErrors.noFirstCapRU);
        String currentErrorNoOtherSmall = substTilte(TitleCaseErrors.noExceptFirstSmallRU);

        doTestBad(chkRU, "название с маленькой буквы", currentErrorNoFirstCap);
        doTestBad(chkRU, "и Совсем Неправильное Название", currentErrorNoFirstCap);

        doTestBad(chkRU, "Какое ТО название", currentErrorNoOtherSmall);
        doTestBad(chkRU, "Тоже название, Однако в середине большая буква", currentErrorNoOtherSmall);
        doTestBad(chkRU, "И еще название: с буквой 'Ё' в середине", currentErrorNoOtherSmall);
        doTestBad(chkRU, "и Совсем Неправильное Название", currentErrorNoOtherSmall);
    }

}
