-- task 3
select    x.post_id from (
    -- row_number() isn't GROUP BY expression but it works
    select      p.post_id, row_number() over (order by p.post_id) r
    from        post p
    
    left        join  comment cm
    on          cm.post_id = p.post_id
    
    group by    p.post_id
    -- when there is no comments, one rec with nulls in cm.* will be selected
    -- because left join
    -- so = 1 is OK too
    having      count(*) <= 1
) x
where       x.r <= 3
order by    x.post_id

/*
result:
1
3
5
*/