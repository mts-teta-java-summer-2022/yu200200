-- task 2
select      p.post_id
from        post p

inner join  comment cm
on          cm.post_id = p.post_id

where       p.title ~ '^[0-9]+'
and         length(p.content) > 20

group by    p.post_id
having      count(*) = 2

order by    p.post_id

/*
result:
22
24
26
28
32
34
36
38
42
44
*/
