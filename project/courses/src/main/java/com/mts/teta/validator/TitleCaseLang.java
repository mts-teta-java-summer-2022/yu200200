package com.mts.teta.validator;

public enum TitleCaseLang  {
    ANY,
    EN,
    RU
}
