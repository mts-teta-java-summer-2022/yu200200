package com.mts.teta.controller;

import com.mts.teta.dto.CourseRequestToCreate;
import com.mts.teta.dto.CourseRequestToUpdate;
import com.mts.teta.dto.ApiError;
import com.mts.teta.dto.FieldValidationError;
import com.mts.teta.entity.Course;
import com.mts.teta.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/course")
@AllArgsConstructor
public class CourseController {
    private final CourseService courseService;

    @GetMapping
    public List<Course> courseTable() {
        return courseService.getAll();
    }

    @GetMapping("/{id}")
    public Course getCourse(@PathVariable("id") Long id) {
        return courseService.getById(id);
    }

    @GetMapping("/find")
    //@PathVariable("id") String prefix
    public List<Course> getByTitlePrefix(
            @RequestParam("field") String field,
            @RequestParam("query") String query
    ) throws Exception {
        return courseService.getByFieldPrefix(field, query);
    }

    @PostMapping
    public Course createCourse(
            @Valid @RequestBody CourseRequestToCreate request
    ) {
        Course course = new Course(request.getAuthor(), request.getTitle());
        return courseService.save(course);
    }

    @PutMapping("/{id}")
    public Course updateCourse(
        @PathVariable Long id,
        @Valid @RequestBody CourseRequestToUpdate request
    ) {
        Course course = courseService.getById(id);
        course.setTitle(request.getTitle());
        course.setAuthor(request.getAuthor());
        courseService.save(course);

        return course;
    }

    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) {
        courseService.delete(id);
    }

}
