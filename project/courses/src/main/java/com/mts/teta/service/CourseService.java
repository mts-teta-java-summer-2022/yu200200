package com.mts.teta.service;

import com.mts.teta.entity.Course;
import com.mts.teta.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public List<Course> getAll() {
        return courseRepository.findAll();
    }

    public Course getById(Long id) {
        // https://stackoverflow.com/questions/52656517/no-serializer-found-for-class-org-hibernate-proxy-pojo-bytebuddy-bytebuddyinterc
        // return courseRepository.getReferenceById(id);
        /*
        try {
            return courseRepository.findById(id).get();
        }
        catch (NoSuchElementException e) {
            throw new NoSuchElementException("Course with id = " + id.toString() + " not found");
        }
        */
        return courseRepository.findById(id).orElseThrow(
                ()->new NoSuchElementException("Course with id = " + id.toString() + " not found")
            );
    }

    public List<Course> getByFieldPrefix(String field, String prf) throws Exception {
        switch (field) {
            case("author"): return getByAuthorPrefix(prf);
            case("title"): return getByTitlePrefix( prf);
            default: throw new Exception("wrong field to find");
        }
    }

    public List<Course> getByAuthorPrefix(String prf) {
        return getAll().stream().filter(
            x->x.getAuthor().toUpperCase().startsWith(prf.toUpperCase())
        ).collect(Collectors.toList());
    }

    public List<Course> getByTitlePrefix(String prf) {
        return getAll().stream().filter(
                x->x.getTitle().toUpperCase().startsWith(prf.toUpperCase())
        ).collect(Collectors.toList());
    }

    public Course save(Course course) {
        return courseRepository.save(course);
    }

    public void delete(Long id) {
        try {
            courseRepository.deleteById(id);
        }
        catch (EmptyResultDataAccessException e) {
            throw new NoSuchElementException("Course with id = " + id.toString() + " not found");
        }
    }

}
