package com.mts.teta.controller;

import com.mts.teta.dto.ApiError;
import com.mts.teta.dto.FieldValidationError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.NoSuchElementException;

@RestControllerAdvice
public class CourseExceptionHanlder {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<?> handleNoSuchElementException(
            Exception ex,
            WebRequest request
    ) {
        return new ResponseEntity<>(
                new ApiError(ex.getMessage()), HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleNoMethodArgumentNotValidException(
            Exception ex,
            WebRequest request
    ) {
        String mess = null;

        if (ex.getMessage().contains("Course title has to be filled") )
            mess = "Course title has to be filled";
        else if (ex.getMessage().contains("Course author has to be filled") )
            mess = "Course author has to be filled";
        else if ( ex.getMessage().contains("TitleCase") )
            mess = FieldValidationError.error;
        else
            mess = ex.getMessage();

        return new ResponseEntity<>(
            new ApiError(mess), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleAllUncaughtException(
        Exception ex,
        WebRequest request
    ) {
        return new ResponseEntity<>(
                new ApiError(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
/*

    @ExceptionHandler
    @ResponseBody
    public ResponseEntity<?> anyExceptionHandler(Exception ex) {
        HttpStatus retStatus;
        String mess = null;

        switch (ex.getClass().getSimpleName()) {
            case("NoSuchElementException"):
                retStatus = HttpStatus.NOT_FOUND;
                break;
            case("MethodArgumentNotValidException"):
                retStatus = HttpStatus.BAD_REQUEST;

                if (ex.getMessage().contains("Course title has to be filled") )
                    mess = "Course title has to be filled";
                else if (ex.getMessage().contains("Course author has to be filled") )
                    mess = "Course author has to be filled";
                else if ( ex.getMessage().contains("TitleCase") )
                    mess = FieldValidationError.error;
                else
                    mess = ex.getMessage();

                break;
            default:
                retStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        mess = (mess != null) ? mess : ex.getMessage();

        return new ResponseEntity<>(
                new ApiError(mess), retStatus
                // new ApiError(ex.getClass().getName()), retStatus
        );
    }
*/
}
