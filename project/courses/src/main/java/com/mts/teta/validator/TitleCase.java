package com.mts.teta.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = TitleCaseImpl.class)
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)

public @interface TitleCase {
    String message() default "{TitleCase}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    TitleCaseLang lang() default TitleCaseLang.ANY;
}
