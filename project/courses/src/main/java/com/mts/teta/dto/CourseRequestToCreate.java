package com.mts.teta.dto;

import com.mts.teta.validator.TitleCase;
import com.mts.teta.validator.TitleCaseLang;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CourseRequestToCreate {
    @NotBlank(message = "Course author has to be filled")
    private String author;
    @NotBlank(message = "Course title has to be filled")
    @TitleCase(lang = TitleCaseLang.RU)
    private String title;
}


