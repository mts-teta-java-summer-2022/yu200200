package com.mts.teta.validator.TitleCaseRule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//если найдено соответствие регулярному выражению REGEX, возвращаем ошибку mess
public class TitleCaseRuleRegExp implements TitleCaseRule {
    private String regex;
    private String mess;

    public TitleCaseRuleRegExp(String regex, String mess) {
        this.regex = regex;
        this.mess = mess;
    };

    public String check(String s) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        if ( matcher.find() )
            return mess;
        else
            return "";
    }
}
