package com.mts.teta.validator.TitleCaseRule;

public class TitleCaseRulePunctuation implements TitleCaseRule {

    private char ch[];  // строка, преобразованная в массив символов
    private int i;      // текущая позиция в строке
    private String error;

    private boolean isQuotation() {
        return (ch[i] == '\'') | (ch[i] == '\"');
    }

    private boolean isComma() {
        return (ch[i] == ',') | (ch[i] == ':');
    }

    private boolean isLetter(int i) {
        return Character.isLetter(ch[i]);
    }

    private void checkQuotation() {
        // если слева от символа стоит буква, считаем, что так можно
        if (  i > 0 ) {
            if (isLetter(i-1))
                return;
        }
        // если справа от символа стоит буква, считаем, что так можно
        if (  i < ch.length - 1 ) {
            if (isLetter(i+1))
                return;
        }

        if (!error.isEmpty())
            error = error + ". ";
        error = error + TitleCaseErrors.punctMotBeforeAfter +  Integer.toString(i);
    }

    private void checkComma() {
        // если слева от символа стоит буква, считаем, что так можно
        if (  i > 0 ) {
            if (isLetter(i-1))
                return;
        }

        if (!error.isEmpty())
            error = error + ". ";
        error = error + TitleCaseErrors.punctMotAfter +  Integer.toString(i);
    }

    public String check(String s) {
        error = "";
        ch  = s.toCharArray();

        for  (i = 0; i < ch.length; i++) {
            if (isQuotation())
                checkQuotation();
            else if (isComma())
                checkComma();
        }

        if (!error.isEmpty())
            error = "%s: " + error;
        return error;
    }
}
