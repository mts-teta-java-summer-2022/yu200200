package com.mts.teta.validator;

import com.mts.teta.validator.TitleCaseRule.*;

import java.util.ArrayList;

public class TitleCaseCheck {
    ArrayList<TitleCaseRule> rules;
    ArrayList<TitleCaseRule> stopOnError;
    String error;

    public TitleCaseCheck(TitleCaseLang lang) {
        rules = new ArrayList<TitleCaseRule>();
        stopOnError = new ArrayList<TitleCaseRule>();

        // проверки ANY - выполняются всегда

        // 1. Символы \r, \t, \n являются запрещенными
        rules.add(new TitleCaseRuleRegExp("\n|\r|\t", TitleCaseErrors.CRLFTAB ) );

        // 2. Слова в заголовках разделяются одним пробелом
        rules.add(new TitleCaseRuleRegExpForTrim("\s{2}", TitleCaseErrors.twoSpaces ) );

        // 3. Строка не должна ни заканчиваться, ни начинаться с пробела
        rules.add(new TitleCaseRuleRegExp("^\s",  TitleCaseErrors.startSpace) );
        rules.add(new TitleCaseRuleRegExp("\s$",  TitleCaseErrors.endSpace) );

        // 4. Смешение русских и английских символов в строке является недопустимым
        rules.add(new TitleCaseRuleTwoRegExp("[А-Яа-яЁё]", "[A-Za-z]", TitleCaseErrors.mixLangs ) );

        // 5.Символы ", ', ,, : не принадлежат ни к какому языку и допускаются к использованию в любых заголовках.
        // Так как они пишутся вплотную к словам, для простоты можем считать их частью алфавита.
        // Вроде по такой постановке ничего делать не надо, но раз "вплотную к словам", будем проверять, что
        // " и ' должны стоять либо слева, либо справа от буквы, а , и : справа от буквы
        // конечно, это неполная проверка, напрмер, незакрытые кавычки она пропустит
        rules.add(new TitleCaseRulePunctuation());

        // 6. Любые другие небуквенные символы являются запрещенными
        // Будем выполнять только при ANY, при EN и RU будут свои проверки, где
        // оставим соответственно только английские и русские буквы
        if (lang == TitleCaseLang.ANY) {
            rules.add(new TitleCaseRuleRegExp("[^А-Яа-яёЁA-Za-z\s,:'\"]",
                    TitleCaseErrors.badChars )
            );
        }

        // проверки EN - выполняются только при lang = EN в декларации
        if (lang == TitleCaseLang.EN) {
            // 0. Проверим, что только английские буквы и прочие разрешенные символы

            // последующие проверки исходят из того, что буквы только английские,
            // поэтому, если эта проверка не пройдена, дальше не проверяем
            TitleCaseRuleRegExp ruleEN = new TitleCaseRuleRegExp("[^A-Za-z\s,:'\"]",
                    TitleCaseErrors.badCharsEN);
            rules.add(ruleEN);
            stopOnError.add(ruleEN);
            //Первое и последнее слово в заголовке должны быть написаны с большой буквы.
            rules.add(new TitleCaseRuleEnCapInBegAndEnd());

            //Все остальные слова также должны быть написаны с большой буквы, если они не относятся к предлогам или союзам:
            // a, but, for, or, not, the, an
            rules.add(new TitleCaseRuleEnCapExceptPrep());
        }

        // проверки RU - выполняются только при lang = RU в декларации
        if (lang == TitleCaseLang.RU) {
            // 0. Проверим, что только английские буквы и прочие разрешенные символы

            // последующие проверки исходят из того, что буквы только русские,
            // поэтому, если эта проверка не пройдена, дальше не проверяем
            TitleCaseRuleRegExp ruleRU = new TitleCaseRuleRegExp("[^А-Яа-яёЁ\s,:'\"]",
                    TitleCaseErrors.badCharsRU);
            rules.add(ruleRU);
            stopOnError.add(ruleRU);
            // 1. Первое слово должно быть написано с большой буквы, остальные - с маленькой
            rules.add(new TitleCaseRuleRuFirstCapOtherSmall());
        }

    }

    public String doCheck(String s) {
        String error = "";
        String checkResult;

        for (TitleCaseRule rule: rules) {
            checkResult = rule.check(s);

            if (!checkResult.isEmpty()) {
                if (!error.isEmpty())
                    error = error + ". ";
                error = error + checkResult;
            }

            // если при ошибке нет смысла проводить дальнейшие проверки, прекращаем их
            if (        !checkResult.isEmpty()
                    &  stopOnError.contains(rule)
            )
                break;
        };

        if (!error.isEmpty())
            error = error.replace("%s", "Title");

        return error;
    }
}
