package com.mts.teta.validator.TitleCaseRule;

public class TitleCaseErrors {
    public static final String  CRLFTAB = "%s has not contain TAB, CR. LF";
    public static final String  twoSpaces = "Words in %s have not be separated with 2 or more spaces";

    public static final String  startSpace = "%s has not start with space";
    public static final String  endSpace = "%s has not end with space";
    public static final String  mixLangs = "%s has not contain mix of cyrillic and latin letters";

    public static final String  punctMotBeforeAfter = "' or \" has to be just before or after letter at pos ";
    public static final String  punctMotAfter = ", or : has to be just after letter at pos ";
    public static final String  badChars = "%s has contain only letters, spaces, \", ', : and ,";

    public static final String  badCharsEN = "%s has contain only latin letters, spaces, \", ', : and ,";
    public static final String  noFirstLastCapsEN = "First and last words of %s have start with cap letter";
    public static final String  noAllExceptPrepCapsEN = "All the words of %s except a, but, for, or, not, the, an have start with cap letter";

    public static final String  badCharsRU = "%s has contain only cyrillic letters, spaces, \", ', : and ,";
    public static final String  noFirstCapRU = "First word of %s has start with cap letter";
    public static final String  noExceptFirstSmallRU = "All the words of %s after first have start with small letter";

}