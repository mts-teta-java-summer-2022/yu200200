package com.mts.teta.validator.TitleCaseRule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//если найдено соответствие обоим регулярным выражениям REGEX и REGEX2, возвращаем ошибку mess
public class TitleCaseRuleTwoRegExp implements TitleCaseRule {
    private String regex;
    private String regex2;
    private String mess;

    public TitleCaseRuleTwoRegExp(String regex, String regex2, String mess) {
        this.regex = regex;
        this.regex2 = regex2;
        this.mess = mess;
    };

    public String check(String s) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        Pattern pattern2 = Pattern.compile(regex2);
        Matcher matcher2 = pattern2.matcher(s);

        if ( matcher.find() & matcher2.find())
            return mess;
        else
            return "";
    }
}
