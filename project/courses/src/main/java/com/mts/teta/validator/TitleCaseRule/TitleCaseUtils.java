package com.mts.teta.validator.TitleCaseRule;

public class TitleCaseUtils {
    //возвращает true, если строка начинается с заглавной буквы, иначе false
    public static boolean startsFromCap(String word) {
        return Character.isUpperCase(word.charAt(0));
    }
    //возвращает true, если строка начинается со строчной буквы, иначе false
    public static boolean startsFromSmall(String word) {
        return Character.isLowerCase(word.charAt(0));
    }
}
