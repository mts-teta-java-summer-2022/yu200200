package com.mts.teta.validator.TitleCaseRule;

import java.util.Arrays;

public class TitleCaseRuleRuFirstCapOtherSmall implements TitleCaseRule {
    public String check(String s) {
        String[] words = s.trim().replaceAll("[,:'\"]", "").split("\s");
        String error = "";
        String error2 = "";

        if (words.length > 0) {
            if (words[0].length() > 0) {
                if (!TitleCaseUtils.startsFromCap(words[0]))
                    error = TitleCaseErrors.noFirstCapRU;
            }
        }

        for (int i=1; i< words.length; i++) {
            if (words[i].length() > 0) {
                if (!TitleCaseUtils.startsFromSmall(words[i]))
                    error2 = TitleCaseErrors.noExceptFirstSmallRU;
            }
        }

        if (!error.isEmpty() & !error2.isEmpty())
            return error + ". " + error2;
        // если только одна ошибка возникла, она и вернется,
        //если ошибок не было, конкатенация двух пустых строк будет пустой строкой
        else
            return error + error2;
    }
}
