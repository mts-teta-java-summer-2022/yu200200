package com.mts.teta.validator.TitleCaseRule;

import java.util.ArrayList;
import java.util.Arrays;

public class TitleCaseRuleEnCapExceptPrep implements TitleCaseRule {
    static final String [] excl = {"a", "but", "for", "or", "not", "the", "an"};

    public String check(String s) {
        String[] words = s.trim().replaceAll("[,:'\"]", "").split("\s");

        // можно с i+1 до words.length-1, чтобы не дублировать другую проверку
        for (int i=0; i< words.length; i++) {
            if (Arrays.asList(excl).contains(words[i].toLowerCase()))
                continue;
            if (words[i].length() == 0)
                continue;
            if (!TitleCaseUtils.startsFromCap(words[i]))
                return TitleCaseErrors.noAllExceptPrepCapsEN;
        }

        return "";
    }
}
