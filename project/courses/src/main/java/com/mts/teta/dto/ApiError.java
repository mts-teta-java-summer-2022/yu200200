package com.mts.teta.dto;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class ApiError {
    private OffsetDateTime dateOccurred;
    private String message;

    public ApiError(String message) {
        super();
        dateOccurred = OffsetDateTime.now();
        this.message = message;
    }

}