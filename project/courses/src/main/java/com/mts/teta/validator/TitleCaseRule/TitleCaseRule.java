package com.mts.teta.validator.TitleCaseRule;

public interface TitleCaseRule {
    //если проверка строки пройдена, возвращает null
    //если проверка не пройдена, возвращает сообщение об ошибке
    String check(
                String s    // проверяемая строка
    );
}
