package com.mts.teta.validator;

import com.mts.teta.dto.FieldValidationError;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TitleCaseImpl implements ConstraintValidator<TitleCase, String> {
    private TitleCaseLang lang;
    private TitleCaseCheck titleCaseCheck;

    @Override
    public void initialize(TitleCase titleCase) {
        //To change body of implemented methods use File | Settings | File Templates.
        lang = titleCase.lang();
        titleCaseCheck = new TitleCaseCheck(lang);
    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        FieldValidationError.error = titleCaseCheck.doCheck(field);
        return ( FieldValidationError.error.equals("") );
    }
}