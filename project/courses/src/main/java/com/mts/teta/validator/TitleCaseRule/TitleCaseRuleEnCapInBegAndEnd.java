package com.mts.teta.validator.TitleCaseRule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TitleCaseRuleEnCapInBegAndEnd implements TitleCaseRule {
    public String check(String s) {
        String[] words = s.trim().replaceAll("[,:'\"]", "").split("\s");

        if (words.length > 0) {
            if (words[0].length() > 0) {
                if (!TitleCaseUtils.startsFromCap(words[0]))
                    return TitleCaseErrors.noFirstLastCapsEN;
            }

            if (words[words.length - 1].length() > 0) {
                if (!TitleCaseUtils.startsFromCap(words[words.length - 1]))
                    return TitleCaseErrors.noFirstLastCapsEN;
            }
        }

        return "";
    }
}
