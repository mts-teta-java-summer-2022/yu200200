package com.mts.teta.validator.TitleCaseRule;

// раз уж написано, что более одного пробела не может быть _между словами_,
// значит пробелы в начале и конце не надо смотреть,
// тем более что для них есть другая ошибка
public class TitleCaseRuleRegExpForTrim extends TitleCaseRuleRegExp {
    public TitleCaseRuleRegExpForTrim(String REGEX, String mess) {
        super(REGEX, mess);
    };

    @Override
    public String check(String s) {
        return super.check(s.trim());
    };
}
