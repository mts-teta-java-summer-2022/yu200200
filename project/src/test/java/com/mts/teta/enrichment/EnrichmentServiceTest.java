package com.mts.teta.enrichment;

import com.mts.teta.enrichment.service.AddObjs;
import com.mts.teta.enrichment.service.EnrichmentServiceImpl;
import com.mts.teta.enrichment.storage.MSISDNRecord;
import com.mts.teta.enrichment.storage.MSISDNStorage;
import com.mts.teta.enrichment.model.Message;
import com.mts.teta.enrichment.model.log.MessageLog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class EnrichmentServiceTest {

    private MessageLog logGood;
    private MessageLog logBad;
    private MSISDNStorage storage;
    private AddObjs addObjs;

    // список сообщений, которые, как ожидается, можно обогатить
    private ConcurrentHashMap <Message, String> mapGood;
    // список сообщений, которые, как ожидается, нельзя обогатить
    private ConcurrentHashMap <Message, String> mapBad;

    //размер хранилища с ФИО
    private static final int storageSize = 100000;

    //счетчик для тестирования добавления в хранилище
    private AtomicInteger count;

    private ConcurrentHashMap<String, Object> locks;

    Object getLock(String s) {
        locks.putIfAbsent(s, new Object());
        return locks.get(s);
    }

    //получение следующего значения счетчика
    private int getNextCounter() {
        return count.incrementAndGet();
    }

    //внесение случайного изменения в хранилище
    public void RandomModifyStorage() {
        int rnd;
        String key;
        String fname;
        String lname;
        int nextCnt;
        Random random = new Random();

        rnd = random.nextInt(3);

        // добавление
        if (rnd == 0) {
            nextCnt = getNextCounter();
            key = fmt(nextCnt);
        }
        // обновление/удаление
        else {
            nextCnt = -1; // чтобы не было ошибки компиляции
            key = fmt(random.nextInt(storageSize));
        }

        synchronized (getLock(key)) {
            // добавление
            if (rnd == 0)
                addToStorage(nextCnt);
                //обновление или удаление - надо выбрать случайный ключ
                //чтобы не блокировать, будем выбирать его среди первоначально заполненных элементов
                //если его нет, значит, ничего не делаем
            else {
                key = fmt(random.nextInt(storageSize));
                //обновление; если после получения ключа его успели удалить, будет добавление
                if (rnd == 1) {
                    int base = random.nextInt(1000000);
                    String newFName = getNewFName(base);
                    String newLName = getNewLName(base);
                    storage.put(key, new MSISDNRecord(newFName, newLName));
                }
                //удаление
                else {
                    storage.remove(key);
                }
            }
        }
    }

    private static final String jsonSrcTemplate = "{\n" +
            "    \"id\": \"%id%\",\n" +
            "    \"action\": \"button_click\",\n" +
            "    \"page\": \"book_card\",\n" +
            "    \"msisdn\": \"%msisdn%\"\n" +
            "}";

    private static final String jsonStrExpectedTemplate = "{\n" +
            "    \"id\": \"%id%\",\n" +
            "    \"action\": \"button_click\",\n" +
            "    \"page\": \"book_card\",\n" +
            "    \"msisdn\": \"%msisdn%\",\n" +
            "    \"enrichment\": {\n" +
            "        \"firstName\": \"%fname%\",\n" +
            "        \"lastName\": \"%lname%\"\n" +
            "    }\n" +
            "}";

    private String fmt(int i) {
        return String.format("%06d", i);
    }

    private String getNewFName(int i) {
        return "new fname" + fmt(i);
    }

    private String getNewLName(int i) {
        return "new lname" + fmt(i);
    }

    private String getSrc(String s, int i, String id) {
        return s.replace("%msisdn%", fmt(i)).
                replace("%id%", id);
    }

    private String getExpected(String s, String key, String id) {
        String ret;

        MSISDNRecord rec = storage.get(key);

        ret = s;
        ret = ret.replace("%msisdn%", key); //"fname"+fmt(i)
        ret = ret.replace("%fname%", rec.getFirstName()); //"fname"+fmt(i)
        ret = ret.replace("%lname%", rec.getLastName()); // "lname"+fmt(i)
        ret = ret.replace("%id%", id);

        return ret;
    }
    EnrichmentServiceImpl serv;

    private void addToStorage(int i) {
        String MSISDN;
        String fname;
        String lname;

        MSISDN = fmt(i);
        fname = "fname" + MSISDN;
        lname = "lname" + MSISDN;

        storage.put(MSISDN, new MSISDNRecord(fname, lname));
    }

    private void fillStorage() {
        for (int i = 0; i < storageSize; i++) {
            addToStorage(i);
        }
    }

    @BeforeEach
    void setUp() {
        logGood = new MessageLog();
        logBad = new MessageLog();
        storage = new MSISDNStorage();
        fillStorage();
        AddObjs addObjs = new AddObjs();
        addObjs.put("storage", storage);
        addObjs.put("logGood", logGood);
        addObjs.put("logBad", logBad);
        serv = new EnrichmentServiceImpl(addObjs);
        locks = new ConcurrentHashMap<String, Object>();

        mapGood = new ConcurrentHashMap <Message, String>();
        mapBad = new ConcurrentHashMap <Message, String>();
        count =  new AtomicInteger(storageSize - 1);    //после первоначального заполнения там от 1 до 99999
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * При незаполненном поле enrichmentType контент должен остаться без изменений,
     * сообщение должно добавиться в журнал необогащенных сообщений
     */
    @Test
    void testNullEnrichmentType () {
        Message test = new Message("zzz", null);
        String retExpected = test.getContent();
        String ret = serv.enrich(test);
        assertEquals(retExpected, ret);
        assertEquals(test, logBad.get(0));
    }

    /**
     * При MSISDN в поле enrichmentType и контенте не в формате JSON
     * контент должен остаться без изменений,
     * сообщение должно добавиться в журнал необогащенных сообщений
     */
    @Test
    void testNotJSONContent () {
        Message test = new Message("zzz", Message.EnrichmentType.MSISDN);
        String retExpected = test.getContent();
        String ret = serv.enrich(test);
        assertEquals(retExpected, ret);
        assertEquals(test, logBad.get(0));
    }

    /**
     * При MSISDN в поле enrichmentType и контенте в формате JSON,
     * если не заполнено поле msisdn,
     * контент должен остаться без изменений,
     * сообщение должно бавиться в журнал необогащенных сообщений
     */
    @Test
    void testNoMsisdn () {
        String jsonStr = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "}";
        Message test = new Message(jsonStr, Message.EnrichmentType.MSISDN);
        String retExpected = test.getContent();
        String ret = serv.enrich(test);
        assertEquals(retExpected, ret);
        assertEquals(test, logBad.get(0));
    }

    /**
     * При MSISDN в поле enrichmentType и контенте в формате JSON,
     * если поле msisdn заполнено, но отсутствует в словаре соответствий MSISDN и ФИО,
     * контент должен остаться без изменений,
     * сообщение должно добавиться в журнал необогащенных сообщений
     */
    @Test
    void testNotExistingMsisdn () {
        String jsonStr = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"88005553535\"\n" +
                "}";
        Message test = new Message(jsonStr, Message.EnrichmentType.MSISDN);
        String retExpected = test.getContent();
        String ret = serv.enrich(test);
        assertEquals(retExpected, ret);
        assertEquals(test, logBad.get(0));
    }

    /**
     * При MSISDN в поле enrichmentType и контенте в формате JSON,
     * если поле msisdn заполнено и присутствует в словаре соответствий MSISDN и ФИО,
     * контент должен расшириться полем enrichment с именем и фамилией,
     * сообщение должно добавиться в журнал обогащенных сообщений
     */
    @Test
    void testExistingMsisdn () throws org.json.JSONException{
        String jsonStr = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"000011\"\n" +
                "}";
        String jsonStrExpected = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"000011\",\n" +
                "    \"enrichment\": {\n" +
                "        \"firstName\": \"fname000011\",\n" +
                "        \"lastName\": \"lname000011\"\n" +
                "    }\n" +
                "}";
        Message test = new Message(jsonStr, Message.EnrichmentType.MSISDN);
        String ret = serv.enrich(test);
        JSONAssert.assertEquals(jsonStrExpected, ret, JSONCompareMode.STRICT);
        assertEquals(test, logGood.get(0));
    }

    /**
     * При MSISDN в поле enrichmentType и контенте в формате JSON,
     * если поле msisdn заполнено и присутствует в словаре соответствий MSISDN и ФИО,
     * при этом поле enrichment уже присутствует
     * поле enrichment в контенте должно замениться полем enrichment с именем и фамилией,
     * сообщение должно добавиться в журнал обогащенных сообщений
     */
    @Test
    void testExistingMsisdnExistingEnrichment() throws org.json.JSONException{
        String jsonStr = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"000012\"\n" +
                "    \"enrichment\": {\n" +
                "        \"xxx\": \"1\",\n" +
                "        \"yyy\": \"2\"\n" +
                "        \"zzz\": \"3\"\n" +
                "    }\n" +
                "}";
        String jsonStrExpected = "{\n" +
                "    \"action\": \"button_click\",\n" +
                "    \"page\": \"book_card\",\n" +
                "    \"msisdn\": \"000012\",\n" +
                "    \"enrichment\": {\n" +
                "        \"firstName\": \"fname000012\",\n" +
                "        \"lastName\": \"lname000012\"\n" +
                "    }\n" +
                "}";
        Message test = new Message(jsonStr, Message.EnrichmentType.MSISDN);
        String ret = serv.enrich(test);
        JSONAssert.assertEquals(jsonStrExpected, ret, JSONCompareMode.STRICT);
        assertEquals(test, logGood.get(0));
    }

    public void enrichMass(
            int nAll,
            CountDownLatch c,
            int nSleep
        ) throws InterruptedException {
        String src;
        String mesId;
        String key;
        int iadded;

        for (int i = 0; i < nAll; i++) {
            try {
                mesId =  UUID.randomUUID().toString();
                if (i % 10 != 0) {
                    key = fmt(i);
                    src = getSrc(jsonSrcTemplate, i, mesId);
                }
                else {
                    iadded = i + 100000;
                    key = fmt(iadded);
                    src = getSrc(jsonSrcTemplate, iadded, mesId);
                }

                synchronized (getLock(key)) {

                    Message test = new Message(src, Message.EnrichmentType.MSISDN);

                    serv.enrich(test);

                    // в мапах ключом делаем исходное сообщение,
                    // а значением - ожидаемый после обогащения контент
                    if (storage.containsKey(key)) // (i % 10 != 0)
                        mapGood.put(test, getExpected(jsonStrExpectedTemplate, key, mesId));
                    else
                        mapBad.put(test, test.getContent());
                }

                //при многопоточном запуске добавим случайную задержку
                if ((nSleep != 0) & (i%100 == 0))
                    Thread.sleep((long)(Math.random()*nSleep));
            }
           finally {
                if (c != null)
                    c.countDown();
            }
        }
        System.out.println(mapGood.size());
        System.out.println(mapBad.size());
    }

    //errCnt - количество допустимых ошибок - удобно временно разрешить сколько-то ошибок для отладки
    protected void checkMass(int nGood, int nBad, int errCnt) throws Exception {
        Message mesInLog;
        String retExpected;

        assertEquals(nGood, logGood.getSize()) ;
        assertEquals(nBad, logBad.getSize()) ;

        for (int i = 0; i < nGood; i++) {
            mesInLog = logGood.get(i);
            try {
                retExpected = mapGood.get(mesInLog);
                if (retExpected == null)
                    throw new AssertionError("No good message " + mesInLog.getContent());
                else {
                    JSONAssert.assertEquals(
                            retExpected,
                            mesInLog.getContent(),
                            JSONCompareMode.STRICT
                    );
                }
            }
            catch (AssertionError e) {
                System.out.println("good " + e.getMessage());
                if (errCnt == 0)
                    throw e;
                else
                    errCnt--;
            }
            finally {
                mapGood.remove(mesInLog);
                mapBad.remove(mesInLog);
                //System.out.println(errCnt);
            }
        }

        for (int i = 0; i < nBad; i++) {
            mesInLog = logBad.get(i);
            try {
                retExpected = mapBad.get(mesInLog);
                if (retExpected == null)
                    throw new AssertionError("No bad message " + mesInLog.getContent());
                else {
                    JSONAssert.assertEquals(
                            retExpected,
                            mesInLog.getContent(),
                            JSONCompareMode.STRICT
                    );
                }
            }
            catch (AssertionError e) {
                    System.out.println("bad " + e.getMessage());
                    if (errCnt == 0)
                        throw e;
                    else
                        errCnt--;
                }
            finally {
                    mapGood.remove(mesInLog);
                    mapBad.remove(mesInLog);
                    //System.out.println(errCnt);
                }
           }

        // должно быть однозначное соответствие между logXXX и mapXXX
        // поэтому после удаления из мапы в цикле после цикла записей не должно остаться
        assertEquals(0, mapGood.size()) ;
        assertEquals(0, mapBad.size()) ;
    };

     /**
     * Проверка массового выполнения в 1 поток
     * Передаем 10000 сообщений с типом обогащения MSISDN и заполненным полем msisdn,
     * из них 9000 MSISDN, существующим в хранилище, и 1000 - с несуществующим
     * проверяем, что в журнале обогащенных и необогащенных сообщений соответственно 9000 и 1000 записей
     * и что контент сообщений соовтветствует ожидаемому обогащенному/необогащенном контенту
     */
    @Test
    void testMass() throws Exception {
        enrichMass(10000,null, 0);
        checkMass(9000, 1000, 0);
    }

    /**
     * Проверка массового выполнения в 4 потока с пулом из 4 потоков
     * В каждом потоке по 10000 сообщений с типом обогащения MSISDN и заполненным полем msisdn,
     * из них 9000 MSISDN, существующим в хранилище, и 1000 - с несуществующим
     * проверяем, что в журнале обогащенных и необогащенных сообщений соответственно 36000 и 4000 записей
     * и что контент сообщений соовтветствует ожидаемому обогащенному/необогащенном контенту
     */
    @Test
    void testParalell() throws Exception {
        final int COUNT = 10000;  // количество сообщений, обрабатываемых в одном потоке

        CountDownLatch cdl1 = new CountDownLatch(COUNT);
        CountDownLatch cdl2 = new CountDownLatch(COUNT);
        CountDownLatch cdl3 = new CountDownLatch(COUNT);
        CountDownLatch cdl4 = new CountDownLatch(COUNT);

        ExecutorService executor;
        executor = Executors.newFixedThreadPool(4);

        System.out.println("Starting threads");
        executor.execute(new EnrichThread(cdl1, this, COUNT,"Thread.1"));
        executor.execute(new EnrichThread(cdl2, this, COUNT,"Thread.2"));
        executor.execute(new EnrichThread(cdl3, this, COUNT,"Thread.3"));
        executor.execute(new EnrichThread(cdl4, this, COUNT,"Thread.4"));

        try {
            cdl1.await();
            cdl2.await();
            cdl3.await();
            cdl4.await();
        } catch(InterruptedException exc) { }

        Thread.sleep(1000);
        executor.shutdown();
        System.out.println("Threads are finished");

        checkMass(36000, 4000, 0);
    }

    /**
     * Проверка массового выполнения в 4 потока с пулом из 4 потоков
     * 2 потока обрабатывают сообщения и 2 потока вносят случайные изменения в хранилище
     * Соответственно в каждом потоке по 50000 сообщений или 50000 изменений.
     * Проверяем, что все сообщения попадают в журнал обогащенных/необогащенных сообщений
     * и что контент сообщений соответствует ожидаемому обогащенному/необогащенном контенту
     */
    @Test
    void testParallelModifyStorage() throws Exception{
        int COUNT = 50000;  // количество сообщений, обрабатываемых в одном потоке
        final int  COUNTST = 25000;  // количество изменений хранилища, выполняемых в одном потоке

        CountDownLatch cdl1 = new CountDownLatch(COUNT);
        CountDownLatch cdl2 = new CountDownLatch(COUNT);
        CountDownLatch cdl3 = new CountDownLatch(COUNT);
        CountDownLatch cdl4 = new CountDownLatch(COUNT);

        ExecutorService executor;
        executor = Executors.newFixedThreadPool(4);

        System.out.println("Starting threads");
        executor.execute(new ModifyStorageThread(cdl1, this, COUNT, 50,"Thread.1"));
        executor.execute(new EnrichThread(cdl2, this, COUNT,"Thread.2"));
        executor.execute(new ModifyStorageThread(cdl3, this, COUNT,50,"Thread.3"));
        executor.execute(new EnrichThread(cdl4, this, COUNT,"Thread.4"));

        try {
            cdl1.await();
            cdl2.await();
            cdl3.await();
            cdl4.await();
        } catch(InterruptedException exc) { }

        Thread.sleep(1000);
        executor.shutdown();
        System.out.println("Threads are finished");

        System.out.println("logGood.size() = "+ Integer.toString(logGood.getSize()));
        System.out.println("logBad.size() = "+ Integer.toString(logBad.getSize()));
        /*
        System.out.println("logGood:");
        for (int i = 0; i < logGood.getSize(); i++) {
            System.out.println(logGood.get(i).getContent());
        }
         */

        assertEquals(COUNT*2, mapGood.size()+mapBad.size());
        assertEquals(COUNT*2, logGood.getSize()+logBad.getSize());
        checkMass(logGood.getSize(), logBad.getSize(), 0);
    }

}

class EnrichThread implements Runnable
{
    private String name;
    private CountDownLatch latch;

    private EnrichmentServiceTest serv;
    private int COUNT = 10000;

    EnrichThread(CountDownLatch c, EnrichmentServiceTest s, int cnt, String n)
    {
        latch = c;
        serv = s;
        COUNT = cnt;
        name = n;
        new Thread(this);
    }

    void printMessage(final String templ)
    {
        System.out.println(templ);
    }

    public void run()
    {
        printMessage(name + " started");
        try {
            serv.enrichMass(COUNT, latch, 500);
            printMessage(name + " completed");
        } catch (InterruptedException e) {}
    }
}

class ModifyStorageThread implements Runnable
{
    private String name;
    private CountDownLatch latch;

    private EnrichmentServiceTest serv;
    private int COUNT = 50000;
    private int nSleep;

    ModifyStorageThread(CountDownLatch c, EnrichmentServiceTest s, int cnt, int nsl, String n)
    {
        latch = c;
        serv = s;
        COUNT = cnt;
        nSleep = nsl;
        name = n;
        new Thread(this);
    }

    void printMessage(final String templ)
    {
        System.out.println(templ);
    }

    public void run()
    {
        try {
            printMessage(name + " started");
            for(int i = 0; i < COUNT; i++) {
                serv.RandomModifyStorage();
                latch.countDown();
                if (i % 10 == 0)
                    Thread.sleep((long) (Math.random() * nSleep));
                if (i % 1000 == 0)
                    printMessage(name + " "  + Integer.toString(i) +" processed");
            }
            printMessage(name + " completed");
        } catch (InterruptedException e) {}
    }
}