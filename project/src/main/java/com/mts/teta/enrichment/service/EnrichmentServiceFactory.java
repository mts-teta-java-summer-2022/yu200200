package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public class EnrichmentServiceFactory {
    public static EnrichMesEx createEnrichmentService(Message m, AddObjs a)
        throws Exception
    {
        if (m.getEnrichmentType() == Message.EnrichmentType.MSISDN)
            return new EnrichmentServiceMSISDN(a);
        throw new Exception("Service not found for enrishment type"+ m.getEnrichmentType().toString());
    }
}
