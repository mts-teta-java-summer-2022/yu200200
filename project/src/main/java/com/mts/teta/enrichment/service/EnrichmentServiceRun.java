package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public class EnrichmentServiceRun implements EnrichMes {
        protected AddObjs addObjs;

    EnrichmentServiceRun(AddObjs a) {
        addObjs = a;
    }

    public Message enrichMes(Message message)  {
        // последовательно создадим экземпляры по шаблону Декоратор
        EnrichmentServiceDecorator decorator;
        EnrichMesEx servMain;
        EnrichMesEx servGood;
        EnrichMes servBad;

        servMain = new EnrichmentServiceMain(addObjs);
        servGood = new EnrichmentServiceDecoratorRepGood(servMain, addObjs);
        servBad = new EnrichmentServiceDecoratorRepBad(servGood, addObjs);

        // вызовем метод декоратора - он последовательно вызовет методы нужных классов
        return servBad.enrichMes(message);
    }
}
