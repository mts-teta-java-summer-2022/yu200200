package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public abstract class EnrichmentServiceMes implements EnrichMesEx {
    public abstract Message enrichMes(Message message);
}
