package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;
import com.mts.teta.enrichment.model.log.AddMessageToLog;

public class EnrichmentServiceDecoratorRepGood implements EnrichMesEx
{
    protected EnrichMesEx origin;

    protected AddObjs addObjs;
    protected AddMessageToLog logGood;

    public EnrichmentServiceDecoratorRepGood(EnrichMesEx par, AddObjs a)  {
        origin = par;
        addObjs = a;
        logGood = (AddMessageToLog)addObjs.get("logGood");
    }

    public Message enrichMes(Message message) throws Exception {
        Message ret;
        ret = origin.enrichMes(message);
        if (logGood != null) {
            logGood.add(ret);
        }

        return ret;
    }
}
