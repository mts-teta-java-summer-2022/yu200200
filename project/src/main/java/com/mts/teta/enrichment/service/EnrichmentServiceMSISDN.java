package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.storage.MSISDNRecord;
import com.mts.teta.enrichment.storage.MSISDNStorage;
import com.mts.teta.enrichment.model.Message;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class EnrichmentServiceMSISDN implements EnrichMesEx {
    protected AddObjs addObjs;
    protected MSISDNStorage storage;

    EnrichmentServiceMSISDN(AddObjs a) {
        addObjs = a;
        storage = (MSISDNStorage)addObjs.get("storage");
    }

    @Override
    public Message enrichMes(Message message) throws Exception {
        String s;
        JSONParser parser = new JSONParser();
        MSISDNRecord rec;
        String MSISDN;
        JSONObject enrichmentBody;
        Object obj;

        s = message.getContent();

        try {
            obj = parser.parse(s);
        }
        //если не удалось разобрать контент, перехватываем исключение
        //и переподнимаем, добавляя сообщение
        catch (Exception e) {
            throw new Exception("content is not in JSON format: " + e.toString());
        }

        JSONObject jsonObj = (JSONObject) obj;

        MSISDN = (String)jsonObj.get("msisdn");
        if (MSISDN == null) {
            //System.out.println("has not MSISDN");
            // выбросим исключение, что нет MSISDN, перехватится в service.EnrichmentServiceDecoratorRepBad
            throw new Exception("no MSISDN field");
        }
        else {
            //System.out.println("has MSISDN = "+ MSISDN);
            rec = storage.get(MSISDN);
            if (rec == null) {
                //System.out.println("rec not found");
                // выбросим исключение, что MSISDN, перехватится в service.EnrichmentServiceDecoratorRepBad
                throw new Exception("data for enrichment not found for MSISDN = " + MSISDN);
            }
            else {
                enrichmentBody = new JSONObject();
                enrichmentBody.put("firstName", rec.getFirstName());
                enrichmentBody.put("lastName", rec.getLastName());

                jsonObj.put("enrichment", enrichmentBody);
                message.setContent(jsonObj.toJSONString());
            }
        }

        return message; // пока что
    }
}
