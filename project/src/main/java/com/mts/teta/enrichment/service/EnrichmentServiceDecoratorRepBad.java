package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;
import com.mts.teta.enrichment.model.log.AddMessageToLog;

public class EnrichmentServiceDecoratorRepBad implements EnrichMes {
    protected AddMessageToLog logBad;
    protected EnrichMesEx origin;
    protected AddObjs addObjs;

    public EnrichmentServiceDecoratorRepBad(EnrichMesEx par, AddObjs a)  {
        origin = par;
        addObjs = a;
        logBad = (AddMessageToLog)addObjs.get("logBad");
    }

    @Override
    public Message enrichMes(Message message)  {
        Message ret;

        try {
            ret = origin.enrichMes(message);
            return ret;
        }
        catch (Exception e) {
            if (logBad != null) {
                logBad.add(message);
            }
            return message;
        }
     }
}
