package com.mts.teta.enrichment.model.log;

import com.mts.teta.enrichment.model.Message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageLog implements AddMessageToLog {
    protected List<Message> list;

    public MessageLog() {
        list = Collections.synchronizedList(new ArrayList<Message>());
    }

    public void add(Message message) {
        list.add(message);
    }
    public Message get(int index) { return list.get(index); }

    public int getSize() {return list.size();}
}
