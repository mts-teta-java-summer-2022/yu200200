package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public interface EnrichmentService {
    String enrich(Message message);
}
