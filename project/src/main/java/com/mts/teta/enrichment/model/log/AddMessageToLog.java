package com.mts.teta.enrichment.model.log;

import com.mts.teta.enrichment.model.Message;

public interface AddMessageToLog {
    void add(Message m);
}
