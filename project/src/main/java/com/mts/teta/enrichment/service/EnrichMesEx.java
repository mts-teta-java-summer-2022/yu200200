package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public interface EnrichMesEx {
    // возвращается само сообщение, а не контент, иначе придется
    // при вызове реализаций постоянно преобразовывать между строкой и com.mts.teta.enrichment.model.Message
    Message enrichMes(Message message) throws Exception;
}