package com.mts.teta.enrichment.model;

public class Message {
    private String content;
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    private EnrichmentType enrichmentType;
    public EnrichmentType getEnrichmentType() {
        return enrichmentType;
    }
    public void setEnrichmentType(EnrichmentType enrichmentType) {
        this.enrichmentType = enrichmentType;
    }

    public Message() {};
    public Message(String content, EnrichmentType enrichmentType) {
        this.content = content;
        this.enrichmentType = enrichmentType;
    }

    public enum EnrichmentType {
        MSISDN
    }
}
