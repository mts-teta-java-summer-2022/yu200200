package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public class EnrichmentServiceImpl implements EnrichmentService {

    protected AddObjs addObjs;
    public EnrichmentServiceImpl(AddObjs a) {
        addObjs = a;
    }

    @Override
    public String enrich(Message message) {
        return new EnrichmentServiceRun(addObjs).enrichMes(message).getContent();
    };
}
