package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public abstract class EnrichmentServiceDecorator implements EnrichMes {
    protected EnrichMes enrichServ;

    public EnrichmentServiceDecorator (EnrichMes par) {
        enrichServ = par;
    }
    @Override
    public Message enrichMes(Message message) {
        return enrichServ.enrichMes(message);
    }
}
