package com.mts.teta.enrichment.service;

import com.mts.teta.enrichment.model.Message;

public class EnrichmentServiceMain implements EnrichMesEx {
    protected AddObjs addObjs;

    EnrichmentServiceMain(AddObjs a) {
        addObjs = a;
    }
    public Message enrichMes(Message message) throws Exception {
        Message ret;
        EnrichMesEx worker;

        worker = EnrichmentServiceFactory.createEnrichmentService(message, addObjs);
        ret = worker.enrichMes(message);

        return ret;
    }
}
