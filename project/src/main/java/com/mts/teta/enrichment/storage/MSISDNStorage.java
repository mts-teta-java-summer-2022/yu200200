package com.mts.teta.enrichment.storage;

import java.util.concurrent.ConcurrentHashMap;

public class MSISDNStorage  {

    protected ConcurrentHashMap <String, MSISDNRecord> data;

    public MSISDNStorage() {
        data = new ConcurrentHashMap <String, MSISDNRecord>();
    }
    public void put(String k, MSISDNRecord v) {
        data.put(k, v);
    }

    public MSISDNRecord get(String k) {
        return data.get(k);
    }

    public void remove(String k) { data.remove(k); }

    public boolean containsKey(String k) {return data.containsKey(k);}

}
