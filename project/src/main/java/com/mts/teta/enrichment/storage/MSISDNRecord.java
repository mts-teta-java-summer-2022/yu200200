package com.mts.teta.enrichment.storage;

public class MSISDNRecord {
    protected String firstName;
    protected String lastName;

    public MSISDNRecord(String fname, String lname) {
        firstName = fname;
        lastName = lname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
